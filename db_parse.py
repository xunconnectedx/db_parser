# -*- coding: cp1251 -*-
import mysql.connector#3306
import psycopg2#5432
from mysql.connector import Error
import pymongo

class ParseDB:
	host = ''
	port = ''
	db_name = ''
	db_type = ''
	login = ''
	password = ''
	conn = None
	
	def __init__(self,host,port,db_type,db_name,login,password):
		self.host = host
		self.port = port
		self.db_name = db_name
		self.db_type = db_type
		self.login = login
		self.password = password
		if self.db_type == 'mysql':
			self.connect_MySQL()
		elif self.db_type == 'psql':
			self.connect_PSQL()

	#Подключение к базеданных MySQL
	def connect_MySQL(self):
		##Передаем в параметрах имя базы (кастом), айпи, юзер+пасс###
		""" Connect to MySQL database """
		try:
			self.conn = mysql.connector.connect(host=self.host,
										   database=self.db_name,
										   port=self.port,# mysql 
										   user=self.login,
										   password=self.password)
			if self.conn.is_connected():
				print 'Connected to MySQL database '+self.db_name
		except Error as e:
			print(e)
			exit()

	
	# def connect_mongo(self):
	# 	try:
	# 		self.conn = pymongo.MongoClient(self.host, self.port)[self.db_name].authenticate(self.login, self.password)
	# 	except pymongo.errors.PyMongoError:
	# 		sys.stderr.write("{0}PyMongo Unexpected Error!\n".format(_ERS))

	#Подключение к базеданных PSQL		
	def connect_PSQL(self):
		try:
			self.conn = psycopg2.connect(host=self.host,
									dbname=self.db_name,
									port=self.port,
									user=self.login,
									password=self.password)
			if psycopg2.extensions.STATUS_READY == self.conn.status:#проверяем статус подключения
				print 'Connected to PostgreSQL database '+self.db_name
			#d = self.conn.get_dsn_parameters()
		except psycopg2.Error as e:
			print(e)
			exit()

	#смена бд
	def change_PSQL_db(self,db_name):
		self.conn.close()
		self.db_name=db_name
		self.connect_PSQL()

	#Получение списка бд
	def get_dbs(self):
		if self.db_type=='mysql':
			return self.get_db_MySQL()
		elif self.db_type == 'psql':
			return self.get_db_PSQL()

	#~||~ MySQL
	def get_db_MySQL(self):
		cursor = None
		try:
			cursor = self.conn.cursor()
			cursor.execute("show databases;")#вывод списка бд
			rows = cursor.fetchall()
			print 'Total dbs:', cursor.rowcount
			for row in rows:
				print row[0]#выводим имена бд
			print ''
			return rows #возвращаем список бд
		except Error as e:
			print e

	#~||~ PSQL
	def get_db_PSQL(self):
		cursor = None
		try:
			cursor = self.conn.cursor()
			cursor.execute("SELECT datname FROM pg_database;");#вывод списка бд
			rows = cursor.fetchall()
			print 'Total dbs:', cursor.rowcount
			for row in rows:
				print row[0]
			print ''
			return rows #возвращаем список бд
		except psycopg2.Error as e:
			print(e)
			exit()


	#Получение списка таблиц базы данных db_name
	def get_tables(self, db_name):
		if self.db_type=='mysql':
			return self.get_tables_MySQL(db_name)
		elif self.db_type == 'psql':
			return self.get_tables_PSQL(db_name)

	#~||~ MySQL 
	def get_tables_MySQL(self, db_name):
		cursor = None
		try:
			cursor = self.conn.cursor()
			cursor.execute("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '%s';"%(db_name))#запрашиваем список таблиц
			rows = cursor.fetchall()
			print 'Total tables in DB "'+db_name+'":', cursor.rowcount
			for row in rows:
				print row[0]#выводим имена таблиц
			print ''
			return rows #возвращаем список таблиц
		except Error as e:
			print e

	#~||~ PSQL 
	def get_tables_PSQL(self,db_name):
		cursor = None
		try:
			self.change_PSQL_db(db_name)#меняем бд реконектом
			cursor = self.conn.cursor()
			cursor.execute("SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = 'public';")#запрашиваем список таблиц
			rows = cursor.fetchall()
			print 'Total tables in DB "'+db_name+'":', cursor.rowcount
			for row in rows:
				print row[0]
			print ''
			return rows #возвращаем список таблиц
		except psycopg2.Error as e:
			print(e)
			exit()

	#Получить список колонок
	def get_columns(self, db_name, table):
		if self.db_type=='mysql':
			return self.get_columns_MySQL(table)
		elif self.db_type == 'psql':
			return self.get_columns_PSQL(table)

	#сбор колонок MySQL
	def get_columns_MySQL(self,table):
		try:
			cursor = self.conn.cursor()
			cursor.execute("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '%s';"%(table))
			rows = cursor.fetchall()
			print 'Total columns in table "'+table+'":', cursor.rowcount
			for row in rows:
				print row[0]#вывод названий колонок
			print ''
			return rows #возвращаем список колонок
		except Error as e:
			print e

	# колонки PSQL
	def get_columns_PSQL(self,table):
		try:
			cursor = self.conn.cursor()
			cursor.execute("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '%s';"%(table))
			rows = cursor.fetchall()
			print 'Total columns in table "'+table+'":', cursor.rowcount
			for row in rows:
				print row[0]#вывод названий колонок
			print ''
			return rows #возвращаем список колонок
		except psycopg2.Error as e:
			print(e)
			exit()
	
	#вывод данных		
	def get_data(self,db,table,columns):
		if self.db_type=='mysql':
			return self.get_data_MySQL(db,table,columns)
		elif self.db_type == 'psql':
			return self.get_data_PSQL(db,table,columns)		

	#вывод данных из db, таблицы table, колонок columns
	def get_data_MySQL(self,db,table,columns):
		try:
			cursor = self.conn.cursor()
			qstr=','.join(columns)#разбиваем массив
			cursor.execute("SELECT %s FROM %s;"%(qstr,table))#формируем запрос
			rows = cursor.fetchall()
			print ''
			print 'DB:',db,'Table:',table,'Col:',columns
			for col in columns: print '%+5s'%(col),
			print ''
			for row in rows:
				for row1 in row:
					print '%+5s'%(row1),
				print ''
			return rows #возвращаем рез-т запроса
		except Error as e:
			print e

	#вывод данных из db, таблицы table, колонок columns
	def get_data_PSQL(self,db,table,columns):
		try:
			self.change_PSQL_db(db)
			cursor = self.conn.cursor()
			qstr=','.join(columns)
			cursor.execute("SELECT %s FROM %s;"%(qstr,table))
			rows = cursor.fetchall()
			print ''
			print 'DB:',db,'Table:',table,'Col:',columns
			for col in columns: print '%+5s'%(col),
			print ''
			for row in rows:
				for row1 in row:
					print '%+5s'%(row1),
				print ''
			return rows #возвращаем рез-т запроса
		except psycopg2.Error as e:
			print(e)
			exit()

	def create_user_MySQL(self,login,password):
		try:
			cursor = self.conn.cursor()
			res = cursor.execute("CREATE USER '%s'@'%s'"%(login,self.host))
			print "User creation returned", res
			res = cursor.execute("SET PASSWORD FOR '%s'@'%s' = PASSWORD('%s')"%(login,self.host,password))
			print "Setting of password returned", res
			res = cursor.execute("GRANT ALL ON *.* TO '%s'@'%s' WITH GRANT OPTION"%(login, self.host))
			print "Granting of privileges returned", res
		except Error as e:
			print e 

if __name__ == '__main__':
    #pars = ParseDB(host,port,db_type,db_name,login,password)#создаем экземпляр класса, который подлючается к бд
    #pars = ParseDB('127.0.0.1','5432','psql','postgres','postgres','postgres')
    pars = ParseDB('127.0.0.1','3306','mysql','test','root','')
    pars.get_dbs()
    pars.create_user_MySQL('batya','2222ttt8')
    #pars.get_tables('test')
    #pars.get_columns('test', 'table2')
    #pars.get_data('test', 'table1', col)

    #pars.get_tables('postgredb')
    #pars.get_columns('postgredb','ptable1') #указываем бд, указываем таблицу
    #pars.get_data('postgredb','ptable1', ['id','name'])

    #\connect database_name - смена бд в пострге