# db_parser {MySQL, PostgreSQL}

Using:

###initial class object###
ParseDB(host,port,db_type,db_name,login,password)
db_type={psql, mysql}

###Get dbs###
get_dbs()

###Get tables###
get_tables(db_name)

###Get columns###
get_columns(db_name, table_name)

###Get data###
get_data(db_name, table_name, ['array', 'of', 'columns', 'names'])

###Create user (MySQL)###
create_user_MySQL(login,pass)


###Some examples###
input:
	
	pars = ParseDB('127.0.0.1','5432','psql','postgres','postgres','postgres')
	pars.get_dbs()
	pars.get_tables('postgredb')
	pars.get_columns('postgredb','ptable1') #указываем бд, указываем таблицу
	pars.get_data('postgredb','ptable1', ['id','name'])

output:

	Connected to PostgreSQL database postgres
	Total dbs: 4
	template1
	template0
	postgres
	postgredb

	Connected to PostgreSQL database postgredb
	Total tables in DB "postgredb": 1
	ptable1

	Total columns in table "ptable1": 2
	id
	name

	Connected to PostgreSQL database postgredb

	DB: postgredb Table: ptable1 Col: ['id', 'name']
	   id  name 
		1 vasya 
		2 vasya2 
	[Finished in 0.4s]